﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour
{

    public PlayerControl player;
    // Skrip GameManager untuk mengakses skor maksimal
    [SerializeField]
    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D anotherCollider)
    {
        
        //jika objek tersebut bernama ball
        if(anotherCollider.name == "Ball")
        {
  
            //tambahkan skor kepada pemain
            player.IncrementScore();

            //jika skor belum mencapai maksimal
            if(player.Score < gameManager.maxScore)
            {
                //... restart bola setelah mengenai dinding
                anotherCollider.gameObject.SendMessage("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
            }
        }
    }
}
