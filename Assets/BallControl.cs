﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{

    //Rigidbody2d bola
    private Rigidbody2D rigidBody2D;

    //besarnya gaya awal
    public float xInitialForce;
    public float yInitialForce;

    // Titik asal lintasan bola saat ini
    private Vector2 trajectoryOrigin;

    
    //<modifikasi>
    //tanda bahwa sudah melewati 1 stage (sudah ada 1 gol tercipta)
    private bool goal = false;

    //gaya yang dihasilkan
    private float force;
    //</modifikasi>


    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        trajectoryOrigin = transform.position;
        // Mulai game
        RestartGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ResetBall()
    {
        //reset ke (0,0)
        transform.position = Vector2.zero;

        //reset kecepatan
        rigidBody2D.velocity = Vector2.zero;
    }

    void PushBall()
    {
        //tentukan nilai komponen y dari gaya dorong antara -yInitialForce dan yInitialForce
        float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);


        //<modifikasi>
        //yRandomInitialForce dapat dianggap sebagai Fy dan xInitialForce dapat dianggap sebagai Fx
        //Force dapat kita uraikan menjadi Force = sqrt(Fx^2 + Fy^2), sehingga kita dapat memodifikasi Fx agar Force...
        //...yang dihasilkan sama dengan Force sebelumnya dengan Fy berapapun
        if (!goal)
        {
            //masih awal permainan
            force = Mathf.Sqrt(Mathf.Pow(xInitialForce, 2) + Mathf.Pow(yRandomInitialForce, 2)); // sqrt(Fx^2 + Fy^2)
            goal = true;
        }
        else
        {
            xInitialForce = Mathf.Sqrt(Mathf.Pow(force, 2) - Mathf.Pow(yRandomInitialForce, 2)); // sqrt(F^2 - Fy^2)
        }
        //</modifikasi>


        //tentukan nilai acak antar 0 (inklusif) dan 2 (eksklusif)
        float randomDirection = Random.Range(0, 2);

        //jika nilainya <1 bola bergerak ke kiri
        //jika tidak bola bergerak ke kanan
        if (randomDirection < 1.0f)
        {
            //gunakan gaya untuk menggerakkan bola ini
            rigidBody2D.AddForce(new Vector2(-xInitialForce, yRandomInitialForce));

        }
        else
        {
            rigidBody2D.AddForce(new Vector2(xInitialForce, yRandomInitialForce));
        }
    }

    void RestartGame()
    {
        // Kembalikan bola ke posisi semula
        ResetBall();

        // Setelah 2 detik, berikan gaya ke bola
        Invoke("PushBall", 2);
    }

    // Ketika bola beranjak dari sebuah tumbukan, rekam titik tumbukan tersebut
    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }

    // Untuk mengakses informasi titik asal lintasan
    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }
}
