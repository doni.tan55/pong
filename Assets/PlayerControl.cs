﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    //tombol untuk bergerak ke atas
    public KeyCode upButton = KeyCode.W;

    //tombol untuk bergerak ke bawah
    public KeyCode downButton = KeyCode.S;

    //kecepatan gerak
    public float speed = 10.0f;

    //batas atas dan batas bawah game scene (batas bawah menggunakan minus(-))
    public float yBoundary = 9.0f;

    //Rigidbody2d raket ini
    private Rigidbody2D rigidBody2D;

    //skor pemain
    private int score;

    // Titik tumbukan terakhir dengan bola, untuk menampilkan variabel-variabel fisika terkait tumbukan tersebut
    private ContactPoint2D lastContactPoint;




    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
     

    }

    // Update is called once per frame
    void Update()
    {
        // dapatkan kecepatan raket sekarang
        Vector2 velocity = rigidBody2D.velocity;

        //jika pemain menekan tombol ke atas, beri kecepatan positif
        if (Input.GetKey(upButton))
        {
            velocity.y = speed;
        }
        // Jika pemain menekan tombol ke bawah, beri kecepatan negatif ke komponen y (ke bawah).
        else if (Input.GetKey(downButton))
        {
            velocity.y = -speed;
        }

        // Jika pemain tidak menekan tombol apa-apa, kecepatannya nol.
        else
        {
            velocity.y = 0.0f;
        }

        // Masukkan kembali kecepatannya ke rigidBody2D.
        rigidBody2D.velocity = velocity;



        //dapatkan posisi raket sekarang
        Vector3 position = transform.position;
        // Jika posisi raket melewati batas atas (yBoundary), kembalikan ke batas atas tersebut.
        if (position.y > yBoundary)
        {
            position.y = yBoundary;
        }

        // Jika posisi raket melewati batas bawah (-yBoundary), kembalikan ke batas atas tersebut.
        else if (position.y < -yBoundary)
        {
            position.y = -yBoundary;
        }

        // Masukkan kembali posisinya ke transform.
        transform.position = position;

    }

    // menaikan skor sebanyak 1 poin
    public void IncrementScore()
    {
        score++;

    }

    //mengembalikan skor menjadi 0
    public void ResetScore()
    {
        score = 0;
    }

    //mendapatkan nilai skor
    public int Score
    {
        get { return score; }
    }

    // Untuk mengakses informasi titik kontak dari kelas lain
    public ContactPoint2D LastContactPoint
    {
        get { return lastContactPoint; }
    }

    // Ketika terjadi tumbukan dengan bola, rekam titik kontaknya.
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("Ball"))
        {
            lastContactPoint = collision.GetContact(0);
        }
    }
    
}
